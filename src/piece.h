#pragma once

#include <iostream>
#include <string>
#include <vector>

class Piece {
    private:
    protected:
        std::pair<int,int> position;
        std::string symbol;
        std::string type;
        char color;
        std::vector<std::pair<int,int>> possible_moves;
    public:
        Piece() {
            this->position = {-1,-1};
            this->symbol = " ";
            this->type = " ";
            this->color = ' ';
        }
        Piece(std::string symbol, std::string type, char color, std::pair<int,int> position);
        char returnColor();
        std::string returnType();
        void setPosition(std::pair<int,int> pos);
        void getMoves(std::vector<std::vector<Piece>> board);
        void getMovesPawn(std::vector<std::vector<Piece>> board);
        void getMovesRook(std::vector<std::vector<Piece>> board);
        void getMovesKnight(std::vector<std::vector<Piece>> board);
        void getMovesBishop(std::vector<std::vector<Piece>> board);
        void getMovesQueen(std::vector<std::vector<Piece>> board);
        void getMovesKing(std::vector<std::vector<Piece>> board);
        std::vector<std::pair<int,int>> possibleMoves();
        std::pair<int,int> returnPosition();
        std::string returnSymbol();
        void printMoves();
};

// Classes for pieces and empty spots on the board
class Empty: public Piece {
    public:
        Empty(std::pair<int,int> position);
};

class Pawn: public Piece {
    public:
        Pawn(char color, std::pair<int,int> position);
        void getAllMoves();
        void transformToQueen();
};

class Rook: public Piece {
    public:
        Rook(char color, std::pair<int,int> position);
        void getAllMoves();
};

class Knight: public Piece {
    public:
        Knight(char color, std::pair<int,int> position);
        void getAllMoves();
};

class Bishop: public Piece {
    public:
        Bishop(char color, std::pair<int,int> position);
        void getAllMoves();
};

class Queen: public Piece {
    public:
        Queen(char color, std::pair<int,int> position);
        void getAllMoves();
};

class King: public Piece {
    public:
        King(char color, std::pair<int,int> position);
        void getAllMoves();
};
