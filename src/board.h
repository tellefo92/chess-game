#pragma once

#include "piece.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

class Board {
    private:
        std::vector<std::vector<Piece>> board;
        std::vector<Piece> row;
        std::vector<std::pair<int,int>> next_opponent_moves;
        std::pair<int,int> white_king_pos = {0,4};
        std::pair<int,int> black_king_pos = {7,4};

    public:
        // Starting new game
        Board();
        // Replay each move from board_state and put the board and pieces in that state.
        Board(std::string board_state);
        // Displays board
        void displayBoard(bool turn);
        // Move piece from 'from_pos' to 'to_pos'
        bool movePiece(bool white_turn, std::pair<int,int> from_pos, std::pair<int,int> to_pos);
        // Returns board vector
        std::vector<std::vector<Piece>> returnBoard();
        void nextOpponentMoves(bool white_turn);
        bool whiteKingChecked();
        bool blackKingChecked();
        bool whiteKingMate();
        bool blackKingMate();
};