#pragma once

#include "piece.h"
#include "board.h"
#include <fstream>
#include <ctime>
#include <cassert>
#include "hps/hps.h"
#include <thread>
#include <chrono>

class Game {
    private:
        int mode;
        bool end = false;
        bool concede = false;
        bool white_turn = true;
        bool invalid_input = true;
        std::pair<std::string,std::string> game_data;
        std::vector<std::pair<std::string,std::string>> saved_data;
        //std::pair<std::string,std::vector<int>> game_data;
        //std::pair<std::string,std::vector<std::pair<std::pair<int,int>,std::pair<int,int>>>> game_data;
        //std::vector<std::pair<std::string,std::vector<int>>> saved_games;
        //std::vector<std::pair<std::string,std::vector<std::pair<std::pair<int,int>,std::pair<int,int>>>>> saved_games;
        std::string start_time, moves, moves_copy;
        //std::vector<int> moves;
        //std::vector<std::pair<std::pair<int,int>,std::pair<int,int>>> moves;
        std::vector<std::pair<int,int>> black_moves_next, white_moves_next;
        std::pair<int,int> from_pos, to_pos;
        std::string from_s, to_s;
    public:
        Game();
        void getMode();
        void newGame();
        void replayGame();
        void continueGame();
        void takeInput();
        void save();
        void read();
        void write();

};